package pisarenko.sudoku;

/**
 * Created by pisarenko on 28.04.2016.
 */
public interface MoveRequestValidationFunction {
    ValidationResult validate(int x, int y, int val, int[][] board);
}
