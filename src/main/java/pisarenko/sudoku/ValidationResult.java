package pisarenko.sudoku;

import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class ValidationResult {
    private final boolean valid;
    private final MoveResult reason;

    public ValidationResult(boolean valid, MoveResult reason) {
        this.valid = valid;
        this.reason = reason;
    }
    public boolean isValid() {
        return valid;
    }

    public MoveResult getReason() {
        return reason;
    }
}
