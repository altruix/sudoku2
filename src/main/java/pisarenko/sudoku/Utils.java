package pisarenko.sudoku;

import org.apache.commons.lang.ArrayUtils;

import java.util.Arrays;

/**
 * Created by pisarenko on 28.04.2016.
 */
public final class Utils {
    private Utils() {

    }
    public static void copyArray(final int[][] src, final int[][] target) {
        for (int i=0; i < src.length; i++) {
            for (int j=0; j < src[0].length; j++) {
                target[i][j] = src[i][j];
            }
        }
    }
    public static boolean boardComplete(final int[][] board) {
        if (board.length == 0) {
            return false;
        }
        final long numberOfZeroCells = Arrays.stream(board).filter(row -> ArrayUtils.contains(row, 0)).count();
        return numberOfZeroCells == 0;
    }
    public static boolean invalidNumberInCell(final int numberInCell) {
        return (numberInCell < 0) || (numberInCell > 9);
    }
    public static boolean validNumberInCell(final int numberInCell) {
        return !invalidNumberInCell(numberInCell);
    }
}
