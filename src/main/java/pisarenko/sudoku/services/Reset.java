package pisarenko.sudoku.services;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pisarenko.sudoku.protocol.ResetResponse;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

/**
 * Created by pisarenko on 27.04.2016.
 */
@Path("/Reset")
public class Reset extends AbstractSudokuService {
    private static final Logger LOGGER = LoggerFactory.getLogger(Reset.class);

    @GET
    @Produces("text/plain")
    public String reset(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = createObjectMapper();
            final ResetResponse response = resetLogic();
            return writeValueAsString(mapper, response);
        } catch (final JsonParseException exception) {
            LOGGER.error("", exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error("", exception);
        } catch (final IOException exception) {
            LOGGER.error("", exception);
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        }
        return "";
    }

    protected ResetResponse resetLogic() {
        this.getPersistence().reset();
        final ResetResponse response = new ResetResponse();
        response.setBoard(this.getPersistence().getCurrentState());
        return response;
    }

}
