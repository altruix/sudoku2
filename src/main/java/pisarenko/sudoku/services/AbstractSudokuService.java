package pisarenko.sudoku.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pisarenko.sudoku.SudokuPersistence;

/**
 * Created by pisarenko on 27.04.2016.
 */
public class AbstractSudokuService {
    private SudokuPersistence persistence;

    public SudokuPersistence getPersistence() {
        return persistence;
    }


    public void setPersistence(final SudokuPersistence prs) {
        this.persistence = prs;
    }

    protected ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }

    protected String writeValueAsString(final ObjectMapper mapper,
                                        final Object response) throws JsonProcessingException {
        return mapper.writeValueAsString(response);
    }
}
