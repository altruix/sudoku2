package pisarenko.sudoku.services;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pisarenko.sudoku.DefaultSudokuMoveValidator;
import pisarenko.sudoku.SudokuMoveValidator;
import pisarenko.sudoku.protocol.MoveRequest;
import pisarenko.sudoku.protocol.MoveResponse;
import pisarenko.sudoku.protocol.MoveResult;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

/**
 * Created by pisarenko on 28.04.2016.
 */
@Path("/Move")
public class Move extends AbstractSudokuService {
    private static final Logger LOGGER = LoggerFactory.getLogger(Move.class);

    private final SudokuMoveValidator validator;

    public Move(final SudokuMoveValidator validator) {
        this.validator = validator;
    }

    public Move() {
        this(new DefaultSudokuMoveValidator());
    }

    @GET
    @Produces("text/plain")
    public String move(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = createObjectMapper();
            final MoveRequest request = readRequest(aRequest, mapper);
            final MoveResponse response = moveLogic(request);
            return writeValueAsString(mapper, response);
        } catch (final JsonParseException exception) {
            LOGGER.error("", exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error("", exception);
        } catch (final IOException exception) {
            LOGGER.error("", exception);
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        }
        return "";
    }

    protected MoveResponse moveLogic(final MoveRequest request) {
        final int[][] board = this.getPersistence().getCurrentState();
        final MoveResult res = validator.validate(request.getX(), request.getY(), request.getNumber(), board);
        if ((res == MoveResult.MOVE_VALID_BOARD_COMPLETE) ||
            (res == MoveResult.MOVE_VALID_BOARD_INCOMPLETE)) {
            this.getPersistence().putNumber(request.getX(), request.getY(), request.getNumber());
        }
        final MoveResponse response = new MoveResponse();
        response.setResult(res);
        return response;
    }

    protected MoveRequest readRequest(final String aRequest, final ObjectMapper mapper) throws IOException {
        return mapper.readValue(aRequest, MoveRequest.class);
    }

}
