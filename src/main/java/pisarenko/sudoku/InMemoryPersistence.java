package pisarenko.sudoku;

/**
 * Created by pisarenko on 27.04.2016.
 */
public class InMemoryPersistence implements SudokuPersistence {
    public final static int[][] INITIAL_STATE = new int[][] {
            {7, 0, 0, 0, 4, 0, 5, 3, 0},
            {0, 0, 5, 0, 0, 8, 0, 1, 0},
            {0, 0, 8, 5, 0, 9, 0, 4, 0},

            {5, 3, 9, 0, 6, 0, 0, 0, 1},
            {0, 0, 0, 0, 1, 0, 0, 0, 5},
            {8, 0, 0, 7, 2, 0, 9, 0, 0},

            {9, 0, 7, 4, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 5, 7, 0, 0, 0},
            {6, 0, 0, 0, 0, 0, 0, 5, 0},
    };
    private int[][] state;

    public InMemoryPersistence() {
        reset();
    }

    public void reset() {
        state = createEmptyBoard();
        copyArray(INITIAL_STATE, state);
    }

    protected int[][] createEmptyBoard() {
        return new int[INITIAL_STATE.length][INITIAL_STATE[0].length];
    }

    protected void copyArray(final int[][] src, final int[][] target) {
        Utils.copyArray(src, target);
    }

    public int[][] getCurrentState() {
        final int[][] result = createEmptyBoard();
        copyArray(state, result);
        return result;
    }

    public void putNumber(final int x, final int y, final int n) {
        this.state[x][y] = n;
    }
}
