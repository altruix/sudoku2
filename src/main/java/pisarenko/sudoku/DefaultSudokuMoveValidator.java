package pisarenko.sudoku;

import pisarenko.sudoku.protocol.MoveResult;
import pisarenko.sudoku.validation.*;

import java.util.*;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class DefaultSudokuMoveValidator implements SudokuMoveValidator {
    private final List<MoveRequestValidationFunction> validationFunctions =
            Arrays.asList(new NullBoardValidator(),
                    new BoardCompleteValidator(),
                    new CoordinateValidator(),
                    new BoardSizeValidator(),
                    new NumberInRequestValidator(),
                    new NumberInBoardValidator(),
                    new CellFreeValidator());
    @Override
    public MoveResult validate(final int x, final int y, final int val, final int[][] board) {
        final Optional<ValidationResult> firstFailedValidation = validationFunctions.stream()
                .map(validator -> validator.validate(x, y, val, board))
                .filter(validationResult -> !validationResult.isValid())
                .findFirst();
        if (firstFailedValidation.isPresent()) {
            return firstFailedValidation.get().getReason();
        }
        final int[][] newBoard = new
                int[InMemoryPersistence.INITIAL_STATE.length][InMemoryPersistence.INITIAL_STATE[0].length];
        Utils.copyArray(board, newBoard);
        newBoard[x][y] = val;
        final boolean boardValid = boardValid(newBoard);
        if (!boardValid) {
            return MoveResult.MOVE_INVALID;
        }
        if (Utils.boardComplete(newBoard)) {
            return MoveResult.MOVE_VALID_BOARD_COMPLETE;
        } else {
            return MoveResult.MOVE_VALID_BOARD_INCOMPLETE;
        }
    }

    protected boolean boardValid(final int[][] board) {
        return everyQuadrantCorrect(board)
                && everyRowCorrect(board)
                && everyColumnCorrect(board);
    }

    protected boolean everyColumnCorrect(final int[][] board) {
        final int[][] transposedBoard = transpose(board);
        return everyRowCorrect(transposedBoard);
    }

    protected int[][] transpose(final int[][] board) {
        final int[][] transpose = new int[board.length][board[0].length];
        for (int i=0; i < board.length; i++) {
            for (int j=0; j < board[0].length; j++) {
                transpose[j][i] = board[i][j];
            }
        }
        return transpose;
    }

    protected boolean everyRowCorrect(final int[][] board) {
        return Arrays.stream(board)
                .map(row -> rowCorrect(row))
                .filter(rowCorrect -> rowCorrect == true)
                .count() == board.length;
    }
    protected boolean rowCorrect(final int[] row) {
        final int[] occurrencesByNumber = new int[10];
        for (int i=0; i < occurrencesByNumber.length; i++) {
            occurrencesByNumber[i] = 0;
        }
        for (int i=0; i < row.length; i++) {
            final int cell = row[i];
            occurrencesByNumber[cell]++;
        }
        final boolean rowIncomplete = (occurrencesByNumber[0] > 0);
        boolean allNumbersCorrect = true;
        for (int i=1; (i < occurrencesByNumber.length) && allNumbersCorrect; i++) {
            final int occurrencesOfCurNumber = occurrencesByNumber[i];
            if (rowIncomplete && (occurrencesOfCurNumber > 1)) {
                allNumbersCorrect = false;
            } else if (!rowIncomplete && (occurrencesOfCurNumber != 1)) {
                allNumbersCorrect = false;
            }
        }
        return allNumbersCorrect;
    }

    protected boolean everyQuadrantCorrect(final int[][] board) {
        final List<int[][]> quadrants = extractQuadrants(board);
        return quadrants.stream()
                .filter(quadrant -> quadrantCorrect(quadrant))
                .count() == quadrants.size();
    }

    protected boolean quadrantCorrect(final int[][] quadrant) {
        final int[] row = quadrantToRow(quadrant);
        return rowCorrect(row);
    }

    protected int[] quadrantToRow(int[][] quadrant) {
        return Arrays.stream(quadrant)
                .flatMapToInt(Arrays::stream)
                .toArray();
    }

    protected List<int[][]> extractQuadrants(final int[][] board) {
        if (board.length != InMemoryPersistence.INITIAL_STATE.length) {
            return Collections.emptyList();
        }
        if (board[0].length != InMemoryPersistence.INITIAL_STATE[0].length) {
            return Collections.emptyList();
        }
        final List<int[][]> quadrants = new ArrayList<>(9);
        for (int i=0; i < board.length; i += 3) {
            for (int j=0; j < board[0].length; j += 3) {
                quadrants.add(extractQuadrant(board, i, j));
            }
        }
        return quadrants;
    }

    protected int[][] extractQuadrant(final int[][] board,
                                      final int startx,
                                      final int starty) {
        final int[][] result = new int[3][3];
        for (int i=0; i < result.length; i++) {
            for (int j=0; j < result[0].length; j++) {
                result[i][j] = board[startx + i][starty + j];
            }
        }
        return result;
    }
}
