package pisarenko.sudoku;

import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public interface SudokuMoveValidator {
    MoveResult validate(final int x, final int y, final int val, final int[][] board);
}
