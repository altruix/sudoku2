package pisarenko.sudoku.protocol;

/**
 * Created by pisarenko on 27.04.2016.
 */
public final class ResetResponse {
    private int[][] board;

    public int[][] getBoard() {
        return board;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }
}
