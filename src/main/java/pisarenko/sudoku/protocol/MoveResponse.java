package pisarenko.sudoku.protocol;

/**
 * Created by pisarenko on 28.04.2016.
 */
public final class MoveResponse {
    private MoveResult result;

    public MoveResult getResult() {
        return result;
    }

    public void setResult(MoveResult result) {
        this.result = result;
    }
}
