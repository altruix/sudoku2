package pisarenko.sudoku.protocol;

/**
 * Created by pisarenko on 28.04.2016.
 */
public final class MoveRequest {
    private int x;
    private int y;
    private int number;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
