package pisarenko.sudoku;

/**
 * Created by pisarenko on 27.04.2016.
 */
public interface SudokuPersistence {
    void reset();
    int[][] getCurrentState();
    void putNumber(final int x, final int y, final int n);
}
