package pisarenko.sudoku.validation;

import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class NullBoardValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (board == null) {
            return new ValidationResult(false, MoveResult.BOARD_NULL);
        }
        return new ValidationResult(true, null);
    }
}
