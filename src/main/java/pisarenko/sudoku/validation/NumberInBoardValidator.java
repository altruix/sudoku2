package pisarenko.sudoku.validation;

import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.Utils;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

import java.util.Arrays;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class NumberInBoardValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (containsInvalidNumbers(board)) {
            return new ValidationResult(false, MoveResult.INVALID_NUMBER_IN_CELL);
        }
        return new ValidationResult(true, null);
    }

    private boolean containsInvalidNumbers(final int[][] board) {
        return Arrays.stream(board).mapToInt(row ->
                (int)Arrays.stream(row).filter(cell -> Utils.invalidNumberInCell(cell)).count()
        ).sum() > 0;
    }
}
