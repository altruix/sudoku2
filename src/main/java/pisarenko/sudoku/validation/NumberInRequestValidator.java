package pisarenko.sudoku.validation;

import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.Utils;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class NumberInRequestValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (Utils.invalidNumberInCell(val)) {
            return new ValidationResult(false, MoveResult.INVALID_NUMBER_IN_REQUEST);
        }
        return new ValidationResult(true, null);
    }
}
