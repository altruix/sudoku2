package pisarenko.sudoku.validation;

import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.Utils;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class BoardCompleteValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (Utils.boardComplete(board)) {
            return new ValidationResult(false, MoveResult.BOARD_ALREADY_COMPLETE);
        }
        return new ValidationResult(true, null);
    }
}
