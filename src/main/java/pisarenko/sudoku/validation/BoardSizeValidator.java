package pisarenko.sudoku.validation;

import pisarenko.sudoku.InMemoryPersistence;
import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class BoardSizeValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if ((board.length != InMemoryPersistence.INITIAL_STATE.length) ||
                (board[0].length != InMemoryPersistence.INITIAL_STATE.length)) {
            return new ValidationResult(false, MoveResult.BOARD_HAS_WRONG_DIMENSIONS);
        }
        return new ValidationResult(true, null);
    }
}
