package pisarenko.sudoku.validation;

import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.Utils;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class CellFreeValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (board[x][y] != 0) {
            return new ValidationResult(false, MoveResult.CELL_ALREADY_TAKEN);
        }
        return new ValidationResult(true, null);

    }
}
