package pisarenko.sudoku.validation;

import pisarenko.sudoku.InMemoryPersistence;
import pisarenko.sudoku.MoveRequestValidationFunction;
import pisarenko.sudoku.ValidationResult;
import pisarenko.sudoku.protocol.MoveResult;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class CoordinateValidator implements MoveRequestValidationFunction {
    @Override
    public ValidationResult validate(int x, int y, int val, int[][] board) {
        if (isInvalid(x) || isInvalid(y)) {
            return new ValidationResult(false, MoveResult.INVALID_COORDINATE);
        }
        return new ValidationResult(true, null);
    }

    private boolean isInvalid(final int coord) {
        return (coord < 0) || (coord >= InMemoryPersistence.INITIAL_STATE.length);
    }
}
