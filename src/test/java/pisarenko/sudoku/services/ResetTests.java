package pisarenko.sudoku.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import pisarenko.sudoku.SudokuPersistence;
import pisarenko.sudoku.protocol.ResetRequest;
import pisarenko.sudoku.protocol.ResetResponse;

import java.io.IOException;

/**
 * Created by pisarenko on 27.04.2016.
 */
public final class ResetTests {
    @Test
    public void resetReturnsResultOfResetLogic() throws IOException {
        // Prepare
        final String requestString = "someRequestAsString";
        final Reset out = Mockito.spy(new Reset());
        final ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
        Mockito.doReturn(mapper).when(out).createObjectMapper();
        final ResetRequest request = new ResetRequest();
        final ResetResponse response = new ResetResponse();
        Mockito.doReturn(response).when(out).resetLogic();
        final String responseAsString = "responseAsString";
        Mockito.doReturn(responseAsString).when(out).writeValueAsString(mapper, response);
        // Run method under test
        final String actualResult = out.reset(requestString);
        // Verify
        Mockito.verify(out).createObjectMapper();
        Mockito.verify(out).resetLogic();
        Mockito.verify(out).writeValueAsString(mapper, response);
        Assertions.assertThat(actualResult).isSameAs(responseAsString);
    }
    @Test
    public void resetLogicCallsPersistenceResetAndReturnsBoardState() {
        // Prepare
        final Reset out = Mockito.spy(new Reset());
        final SudokuPersistence persistence = Mockito.mock(SudokuPersistence.class);
        out.setPersistence(persistence);
        final ResetRequest request = new ResetRequest();
        final int[][] currentState = new int[0][0];
        Mockito.when(persistence.getCurrentState()).thenReturn(currentState);
        // Run method under test
        final ResetResponse actualResult = out.resetLogic();
        // Verify
        Assertions.assertThat(actualResult).isNotNull();
        Mockito.verify(persistence).reset();
        Mockito.verify(persistence).getCurrentState();
        Assertions.assertThat(actualResult.getBoard()).isSameAs(currentState);
    }
}
