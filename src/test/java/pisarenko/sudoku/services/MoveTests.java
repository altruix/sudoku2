package pisarenko.sudoku.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;
import pisarenko.sudoku.SudokuMoveValidator;
import pisarenko.sudoku.SudokuPersistence;
import pisarenko.sudoku.protocol.MoveRequest;
import pisarenko.sudoku.protocol.MoveResponse;
import pisarenko.sudoku.protocol.MoveResult;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by pisarenko on 28.04.2016.
 */
public final class MoveTests {
    @Test
    public void moveCallsMoveLogic() throws IOException {
        // Prepare
        final String requestTxt = "requestTxt";
        final Move out = Mockito.spy(new Move());
        final ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
        Mockito.doReturn(mapper).when(out).createObjectMapper();
        final MoveRequest request = new MoveRequest();
        Mockito.doReturn(request).when(out).readRequest(requestTxt, mapper);
        final MoveResponse response = new MoveResponse();
        Mockito.doReturn(response).when(out).moveLogic(request);
        final String responseTxt = "responseTxt";
        Mockito.doReturn(responseTxt).when(out).writeValueAsString(mapper, response);
        // Run method under test
        final String actualResult = out.move(requestTxt);
        // Verify
        Mockito.verify(out).readRequest(requestTxt, mapper);
        Mockito.verify(out).moveLogic(request);
        Mockito.verify(out).writeValueAsString(mapper, response);
        Assertions.assertThat(actualResult).isSameAs(responseTxt);
    }
    @Test
    public void moveLogicUpdatesBoardIfValid() {
        // TODO: Implement this
        moveLogicTestLogic(MoveResult.MOVE_VALID_BOARD_COMPLETE, Mockito.times(1));
        moveLogicTestLogic(MoveResult.MOVE_VALID_BOARD_INCOMPLETE, Mockito.times(1));
    }

    protected void moveLogicTestLogic(final MoveResult validationResult,
                                      final VerificationMode expectedNumberOfBoardUpdates) {
        // Prepare
        final SudokuMoveValidator validator = Mockito.mock(SudokuMoveValidator.class);
        final Move out = new Move(validator);
        final SudokuPersistence persistence = Mockito.mock(SudokuPersistence.class);
        out.setPersistence(persistence);
        final MoveRequest request = new MoveRequest();
        final int x = 1;
        final int y = 2;
        final int number = 1;
        request.setX(x);
        request.setY(y);
        request.setNumber(number);
        final int[][] board = new int[0][0];
        Mockito.when(persistence.getCurrentState()).thenReturn(board);
        Mockito.when(validator.validate(x, y, number, board)).thenReturn(validationResult);
        // Run method under test
        final MoveResponse actualResult = out.moveLogic(request);
        // Verify
        Mockito.verify(persistence).getCurrentState();
        Mockito.verify(validator).validate(x, y, number, board);
        Mockito.verify(persistence, expectedNumberOfBoardUpdates).putNumber(x, y, number);
        Assertions.assertThat(actualResult).isNotNull();
        Assertions.assertThat(actualResult.getResult()).isEqualTo(validationResult);
    }

    @Test
    public void moveLogicDoesNotUpdateBoardIfMoveInvalid() {
        Arrays.stream(MoveResult.values())
                .filter(x ->
                        (x != MoveResult.MOVE_VALID_BOARD_COMPLETE) &&
                        (x != MoveResult.MOVE_VALID_BOARD_INCOMPLETE))
                .forEach(x -> moveLogicTestLogic(x, Mockito.never()));
    }
}
