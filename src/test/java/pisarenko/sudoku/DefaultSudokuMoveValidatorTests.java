package pisarenko.sudoku;

import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import pisarenko.sudoku.protocol.MoveResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pisarenko on 28.04.2016.
 */
public final class DefaultSudokuMoveValidatorTests {
    final static int[][] COMPLETE_BOARD = new int[][] {
            {7, 9, 2, 1, 4, 6, 5, 3, 8},
            {4, 6, 5, 2, 3, 8, 7, 1, 9},
            {3, 1, 8, 5, 7, 9, 6, 4, 2},

            {5, 3, 9, 8, 6, 4, 2, 7, 1},
            {2, 7, 6, 9, 1, 3, 4, 8, 5},
            {8, 4, 1, 7, 2, 5, 9, 6, 3},

            {9, 5, 7, 4, 8, 1, 3, 2, 6},
            {1, 2, 3, 6, 5, 7, 8, 9, 4},
            {6, 8, 4, 3, 9, 2, 1, 5, 7},
    };
    final static int[][] ALMOST_COMPLETE_BOARD = new int[][] {
            {7, 9, 2, 1, 4, 6, 5, 3, 8},
            {4, 6, 5, 2, 3, 8, 7, 1, 9},
            {3, 1, 8, 5, 7, 9, 6, 4, 2},

            {5, 3, 9, 8, 6, 4, 2, 7, 1},
            {2, 7, 6, 9, 1, 3, 4, 8, 5},
            {8, 4, 1, 7, 2, 5, 9, 6, 3},

            {9, 5, 7, 4, 8, 1, 3, 2, 6},
            {1, 2, 3, 6, 5, 7, 8, 9, 4},
            {6, 8, 4, 3, 9, 2, 1, 5, 0},
    };
    final static int[][] EVEN_LESS_COMPLETE_BOARD = new int[][] {
            {0, 9, 2, 1, 4, 6, 5, 3, 8},
            {4, 6, 5, 2, 3, 8, 7, 1, 9},
            {3, 1, 8, 5, 7, 9, 6, 4, 2},

            {5, 3, 9, 8, 6, 4, 2, 7, 1},
            {2, 7, 6, 9, 1, 3, 4, 8, 5},
            {8, 4, 1, 7, 2, 5, 9, 6, 3},

            {9, 5, 7, 4, 8, 1, 3, 2, 6},
            {1, 2, 3, 6, 5, 7, 8, 9, 4},
            {6, 8, 4, 3, 9, 2, 1, 5, 0},
    };
    private static final int[][] QUADRANT_1 = new int[][]{
            {7, 9, 2},
            {4, 6, 5},
            {3, 1, 8},
    };
    private static final int[][] QUADRANT_2 = new int[][]{
            {1, 4, 6},
            {2, 3, 8},
            {5, 7, 9},
    };
    private static final int[][] QUADRANT_3 = new int[][]{
            {5, 3, 8},
            {7, 1, 9},
            {6, 4, 2},
    };
    private static final int[][] QUADRANT_4 = new int[][]{
            {5, 3, 9},
            {2, 7, 6},
            {8, 4, 1},
    };
    private static final int[][] QUADRANT_5 = new int[][]{
            {8, 6, 4},
            {9, 1, 3},
            {7, 2, 5},
    };
    private static final int[][] QUADRANT_6 = new int[][]{
            {2, 7, 1},
            {4, 8, 5},
            {9, 6, 3},
    };
    private static final int[][] QUADRANT_7 = new int[][]{
            {9, 5, 7},
            {1, 2, 3},
            {6, 8, 4},
    };
    private static final int[][] QUADRANT_8 = new int[][]{
            {4, 8, 1},
            {6, 5, 7},
            {3, 9, 2},
    };
    private static final int[][] QUADRANT_9 = new int[][]{
            {3, 2, 6},
            {8, 9, 4},
            {1, 5, 7},
    };

    @Test
    public final void validateDetectsNullBoard() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(1, 2, 3, null);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.BOARD_NULL);
    }
    @Test
    public final void validateDetectsAlreadyCompleteBoard() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(1, 2, 3, COMPLETE_BOARD);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.BOARD_ALREADY_COMPLETE);
    }

    @Test
    public final void validateDetectsNegativeXCoordinate() {
        invalidCoordinateTestLogic(-1, 2);
    }

    protected void invalidCoordinateTestLogic(int x, int y) {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(x, y, 3, InMemoryPersistence.INITIAL_STATE);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.INVALID_COORDINATE);
    }

    @Test
    public final void validateDetectsNegativeYCoordinate() {
        invalidCoordinateTestLogic(1, -1);
    }
    @Test
    public final void validateDetectsTooLargeXCoordinate() {
        invalidCoordinateTestLogic(InMemoryPersistence.INITIAL_STATE.length, 0);
        invalidCoordinateTestLogic(InMemoryPersistence.INITIAL_STATE.length+1, 0);
    }
    @Test
    public final void validateDetectsTooLargeYCoordinate() {
        invalidCoordinateTestLogic(0, InMemoryPersistence.INITIAL_STATE[0].length);
        invalidCoordinateTestLogic(0, InMemoryPersistence.INITIAL_STATE[0].length+1);
    }
    @Test
    public final void validateDetectsWrongSizeOfBoard() {
        wrongDimensionsTestLogic(0, 0);
        wrongDimensionsTestLogic(1, 1);
        wrongDimensionsTestLogic(1, 2);
        wrongDimensionsTestLogic(2, 1);
        wrongDimensionsTestLogic(3, 3);
        wrongDimensionsTestLogic(8, 8);
        wrongDimensionsTestLogic(10, 10);
        wrongDimensionsTestLogic(11, 11);
    }

    protected void wrongDimensionsTestLogic(int xsize, int ysize) {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        final int[][] board = new int[xsize][ysize];
        for (int i=0; i < board.length; i++) {
            for (int j=0; j < board[0].length; j++) {
                board[i][j] = 0;
            }
        }
        // Run method under test
        final MoveResult actualResult = out.validate(0, 0, 1, board);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.BOARD_HAS_WRONG_DIMENSIONS);
    }

    @Test
    public final void validateDetectsWrongNumbersInBoard() {
        wrongNumbersInBoardTestLogic(-1);
        wrongNumbersInBoardTestLogic(10);
        wrongNumbersInBoardTestLogic(11);
    }

    protected void wrongNumbersInBoardTestLogic(final int wrongNumber) {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        final int[][] board = InMemoryPersistence.INITIAL_STATE;
        board[8][8] = wrongNumber;
        // Run method under test
        final MoveResult actualResult = out.validate(0, 0, 1, board);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.INVALID_NUMBER_IN_CELL);
    }

    @Test
    public final void validateDetectsCellAlreadyTaken() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(0, 0, 1, InMemoryPersistence.INITIAL_STATE);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.CELL_ALREADY_TAKEN);
    }
    @Test
    public final void validateDetectsInvalidNumberInRequest() {
        invalidNumberInRequestTestLogic(-2);
        invalidNumberInRequestTestLogic(-1);
        invalidNumberInRequestTestLogic(10);
        invalidNumberInRequestTestLogic(11);
    }

    protected void invalidNumberInRequestTestLogic(int invalidNumber) {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(0, 1, invalidNumber, InMemoryPersistence.INITIAL_STATE);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.INVALID_NUMBER_IN_REQUEST);
    }

    @Test
    public final void validateDetectsInvalidMove() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(8, 8, 8, ALMOST_COMPLETE_BOARD);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.MOVE_INVALID);
    }

    @Test
    public final void validateDetectsValidMoveAfterWhichBoardIsComplete() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(8, 8, 7, ALMOST_COMPLETE_BOARD);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.MOVE_VALID_BOARD_COMPLETE);
    }
    @Test
    public final void validateDetectsValidMoveAfterWhichBoardIsNotComplete() {
        // Prepare
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        // Run method under test
        final MoveResult actualResult = out.validate(0, 0, 7, EVEN_LESS_COMPLETE_BOARD);
        // Verify
        Assertions.assertThat(actualResult).isEqualTo(MoveResult.MOVE_VALID_BOARD_INCOMPLETE);
    }
    @Test
    public void boardValidDetectsValidBoard() {
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        Assertions.assertThat(out.boardValid(COMPLETE_BOARD)).isTrue();
    }
    @Test
    public void everyRowCorrectDetectsValidRows() {
        final int[][] board = new int[][] {
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 6, 5, 2, 3, 8, 7, 1, 9},
                {3, 1, 8, 5, 7, 9, 6, 4, 2},

                {5, 3, 9, 8, 6, 4, 2, 7, 1},
                {2, 7, 6, 9, 1, 3, 4, 8, 5},
                {8, 4, 1, 7, 2, 5, 9, 6, 3},

                {9, 5, 7, 4, 8, 1, 3, 2, 6},
                {1, 2, 3, 6, 5, 7, 8, 9, 4},
                {6, 8, 4, 3, 9, 2, 1, 5, 7},
        };
        Assertions.assertThat(new DefaultSudokuMoveValidator().everyRowCorrect(board)).isTrue();
    }
    @Test
    public void everyRowCorrectDetectsInvalidRows() {
        final int[][] board = new int[][] {
                {1, 1, 3, 4, 5, 6, 7, 8, 9},
                {4, 6, 5, 2, 3, 8, 7, 1, 9},
                {3, 1, 8, 5, 7, 9, 6, 4, 2},

                {5, 3, 9, 8, 6, 4, 2, 7, 1},
                {2, 7, 6, 9, 1, 3, 4, 8, 5},
                {8, 4, 1, 7, 2, 5, 9, 6, 3},

                {9, 5, 7, 4, 8, 1, 3, 2, 6},
                {1, 2, 3, 6, 5, 7, 8, 9, 4},
                {6, 8, 4, 3, 9, 2, 1, 5, 7},
        };
        Assertions.assertThat(new DefaultSudokuMoveValidator().everyRowCorrect(board)).isFalse();
    }
    @Test
    public void everyColumnCorrectDetectsValidBoards() {
        Assertions.assertThat(new DefaultSudokuMoveValidator().everyColumnCorrect(COMPLETE_BOARD)).isTrue();
    }
    @Test
    public void everyColumnCorrectDetectsInvalidBoards() {
        final int[][] board = new int[][] {
                {7, 9, 2, 1, 4, 6, 5, 3, 8},
                {7, 6, 5, 2, 3, 8, 7, 1, 9},
                {3, 1, 8, 5, 7, 9, 6, 4, 2},

                {5, 3, 9, 8, 6, 4, 2, 7, 1},
                {2, 7, 6, 9, 1, 3, 4, 8, 5},
                {8, 4, 1, 7, 2, 5, 9, 6, 3},

                {9, 5, 7, 4, 8, 1, 3, 2, 6},
                {1, 2, 3, 6, 5, 7, 8, 9, 4},
                {6, 8, 4, 3, 9, 2, 1, 5, 7},
        };
        Assertions.assertThat(new DefaultSudokuMoveValidator().everyColumnCorrect(board)).isFalse();
    }
    @Test
    public void transposeDefaultScenario() {
        final int[][] board = new int[][] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        final int[][] expectedTranspose = new int[][] {
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9},
        };
        final int[][] actualTranspose = new DefaultSudokuMoveValidator().transpose(board);
        Assertions.assertThat(actualTranspose).isNotNull();
        Assertions.assertThat(actualTranspose.length).isEqualTo(expectedTranspose.length);
        Assertions.assertThat(actualTranspose[0].length).isEqualTo(expectedTranspose[0].length);
        Assertions.assertThat(actualTranspose).isEqualTo(expectedTranspose);
    }
    @Test
    public void everyQuadrantCorrectDefaultScenario() {
        everyQuadrantCorrectTestLogic(false, false);
        everyQuadrantCorrectTestLogic(true, true);
    }
    private void everyQuadrantCorrectTestLogic(final boolean secondQuadrantCorrect,
                                               final boolean expetedResult) {
        // Prepare
        final DefaultSudokuMoveValidator out = Mockito.spy(new DefaultSudokuMoveValidator());
        final int[][] quadrant1 = new int[][] { {1 }};
        final int[][] quadrant2 = new int[0][0];
        final List<int[][]> quadrants = new ArrayList<>(2);
        quadrants.add(quadrant1);
        quadrants.add(quadrant2);
        final int[][] board = new int[0][0];
        Mockito.doReturn(quadrants).when(out).extractQuadrants(board);
        Mockito.doReturn(true).when(out).quadrantCorrect(quadrant1);
        Mockito.doReturn(secondQuadrantCorrect).when(out).quadrantCorrect(quadrant2);
        // Run method under test
        final boolean result = out.everyQuadrantCorrect(board);
        // Verify
        Mockito.verify(out).extractQuadrants(board);
        Mockito.verify(out).quadrantCorrect(quadrant1);
        Mockito.verify(out).quadrantCorrect(quadrant2);
        Assertions.assertThat(result).isEqualTo(expetedResult);
    }
    @Test
    public void extractQuadrantsDefaultScenario() {
        // Prepare
        final int[][] board = new int[][] {
                {7, 9, 2, 1, 4, 6, 5, 3, 8},
                {4, 6, 5, 2, 3, 8, 7, 1, 9},
                {3, 1, 8, 5, 7, 9, 6, 4, 2},

                {5, 3, 9, 8, 6, 4, 2, 7, 1},
                {2, 7, 6, 9, 1, 3, 4, 8, 5},
                {8, 4, 1, 7, 2, 5, 9, 6, 3},

                {9, 5, 7, 4, 8, 1, 3, 2, 6},
                {1, 2, 3, 6, 5, 7, 8, 9, 4},
                {6, 8, 4, 3, 9, 2, 1, 5, 7},
        };
        // Run method under test
        final List<int[][]> result = new DefaultSudokuMoveValidator().extractQuadrants(board);
        // Verify
        Assertions.assertThat(result).contains(QUADRANT_1);
        Assertions.assertThat(result).contains(QUADRANT_2);
        Assertions.assertThat(result).contains(QUADRANT_3);
        Assertions.assertThat(result).contains(QUADRANT_4);
        Assertions.assertThat(result).contains(QUADRANT_5);
        Assertions.assertThat(result).contains(QUADRANT_6);
        Assertions.assertThat(result).contains(QUADRANT_7);
        Assertions.assertThat(result).contains(QUADRANT_8);
        Assertions.assertThat(result).contains(QUADRANT_9);
        Assertions.assertThat(result.size()).isEqualTo(9);
    }
    @Test
    public void quadrantCorrectDefaultScenario() {
        quadrantCorrectDefaultScenarioTestLogic(false);
        quadrantCorrectDefaultScenarioTestLogic(true);
    }
    @Test
    public void qudrantCorrectDetectsCorrectQuadrants() {
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_1)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_2)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_3)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_4)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_5)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_6)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_7)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_8)).isTrue();
        Assertions.assertThat(out.quadrantCorrect(QUADRANT_9)).isTrue();
    }
    @Test
    public void qudrantCorrectDetectsInvalidQuadrants() {
        int[][] invq1 = new int[][]{
                {7, 9, 2},
                {5, 6, 5},
                {3, 1, 8},
        };
        int[][] invq2 = new int[][]{
                {7, 7, 2},
                {4, 6, 5},
                {3, 1, 8},
        };
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        Assertions.assertThat(out.quadrantCorrect(invq1)).isFalse();
        Assertions.assertThat(out.quadrantCorrect(invq2)).isFalse();
    }
    protected void quadrantCorrectDefaultScenarioTestLogic(final boolean rowCorrect) {
        // Prepare
        final DefaultSudokuMoveValidator out = Mockito.spy(new DefaultSudokuMoveValidator());
        final int[][] quadrant = new int[0][0];
        final int[] row = new int[0];
        Mockito.doReturn(row).when(out).quadrantToRow(quadrant);
        Mockito.doReturn(rowCorrect).when(out).rowCorrect(row);
        // Run method under test
        final boolean actualResult = out.quadrantCorrect(quadrant);
        // Verify
        Mockito.verify(out).quadrantToRow(quadrant);
        Mockito.verify(out).rowCorrect(row);
        Assertions.assertThat(actualResult).isEqualTo(rowCorrect);
    }
    @Test
    public void quadrantToRowDefaultScenario() {
        final DefaultSudokuMoveValidator out = new DefaultSudokuMoveValidator();
        final int[] row = out.quadrantToRow(QUADRANT_1);
        Assertions.assertThat(row).isEqualTo(new int[] {7, 9, 2, 4, 6, 5, 3, 1, 8});
    }
}
