package pisarenko.sudoku;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pisarenko.sudoku.protocol.MoveRequest;
import pisarenko.sudoku.protocol.MoveResponse;
import pisarenko.sudoku.protocol.MoveResult;
import pisarenko.sudoku.protocol.ResetResponse;

import java.io.IOException;
import java.net.URLEncoder;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class IntegrationTests {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationTests.class);

    @Test
    @Ignore
    public void integrationTest() throws IOException {
        final HttpClient httpClient = new DefaultHttpClient();
        final ObjectMapper mapper = new ObjectMapper();
        reset(httpClient, mapper);
        for (int i=0; i < InMemoryPersistence.INITIAL_STATE.length; i++) {
            for (int j=0; j < InMemoryPersistence.INITIAL_STATE[0].length; j++) {
                if ((i == (InMemoryPersistence.INITIAL_STATE.length - 1)) &&
                    (j == (InMemoryPersistence.INITIAL_STATE[0].length - 1))) {
                    break;
                }
                if (InMemoryPersistence.INITIAL_STATE[i][j] != 0) {
                    continue;
                }
                final int number = DefaultSudokuMoveValidatorTests.COMPLETE_BOARD[i][j];
                makeMove(httpClient,
                        mapper,
                        i,
                        j,
                        number,
                        MoveResult.MOVE_VALID_BOARD_INCOMPLETE
                );
            }
        }
        makeMove(httpClient, mapper, 8, 8, 7, MoveResult.MOVE_VALID_BOARD_COMPLETE);
    }

    private void reset(HttpClient httpClient, ObjectMapper mapper) throws IOException {
        final String requestAsString = "";
        final ResetResponse resp = sendResetRequest(httpClient, mapper, requestAsString);
        Assertions.assertThat(resp.getBoard()).isEqualTo(InMemoryPersistence.INITIAL_STATE);
    }

    private ResetResponse sendResetRequest(final HttpClient httpClient,
                                           final ObjectMapper mapper,
                                           final String requestAsString) throws IOException {
        final HttpGet httpGet = new HttpGet(String.format("http://localhost:8080/sudoku/Reset?request=", requestAsString));
        final HttpResponse response = httpClient.execute(httpGet);
        Assertions.assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
        final HttpEntity entity = response.getEntity();
        final String entityStr = EntityUtils.toString(entity);
        return mapper.readValue(entityStr, ResetResponse.class);
    }

    private void makeMove(final HttpClient httpClient,
                          final ObjectMapper mapper,
                          final int x,
                          final int y,
                          final int number, MoveResult expectedResponse) throws IOException {
        final MoveRequest request = new MoveRequest();
        request.setX(x);
        request.setY(y);
        request.setNumber(number);
        final String requestAsString = mapper.writeValueAsString(request);

        final HttpGet httpGet = new HttpGet(
                String.format(
                        "http://localhost:8080/sudoku/Move?request=%s",
                        URLEncoder.encode(requestAsString, "UTF-8")
                )
        );
        final HttpResponse httpResponse = httpClient.execute(httpGet);
        Assertions.assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
        final HttpEntity entity = httpResponse.getEntity();
        final String entityStr = EntityUtils.toString(entity);
        final MoveResponse response = mapper.readValue(entityStr, MoveResponse.class);
        Assertions.assertThat(response.getResult()).isEqualTo(expectedResponse);
    }
}
