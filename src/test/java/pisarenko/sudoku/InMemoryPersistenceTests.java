package pisarenko.sudoku;

import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by pisarenko on 27.04.2016.
 */
public final class InMemoryPersistenceTests {
    @Test
    public void resetResetsTheBoard() {
        // Prepare
        final InMemoryPersistence out = new InMemoryPersistence();
        out.putNumber(0, 0, 1);
        out.putNumber(0, 3, 2);
        out.putNumber(4, 0, 3);
        out.putNumber(8, 8, 4);
        // Run method under test
        out.reset();
        // Verify
        Assertions.assertThat(out.getCurrentState()).isEqualTo(InMemoryPersistence.INITIAL_STATE);
    }
    @Test
    public void copyArrayDefaultScenario() {
        // Prepare
        final InMemoryPersistence out = new InMemoryPersistence();
        final int[][] target = new int[9][9];
        for (int i=0; i < target.length; i++) {
            for (int j=0; j < target[0].length; j++) {
                target[i][j] = -1;
            }
        }
        // Run method under test
        out.copyArray(InMemoryPersistence.INITIAL_STATE, target);
        // Verify
        Assertions.assertThat(target).isEqualTo(InMemoryPersistence.INITIAL_STATE);
    }
    @Test
    public void getCurrentStateReturnsCopyOfState() {
        // Prepare
        final InMemoryPersistence out = Mockito.spy(new InMemoryPersistence());
        final int[][] callInResetResult = new int[9][9];
        final int[][] callIngetCurrentStateResult = new int[9][9];
        Mockito
                .doReturn(callInResetResult)
                .doReturn(callIngetCurrentStateResult)
                .when(out)
                .createEmptyBoard();
        // Verify that createEmptyBoard in reset was called
        out.reset();
        Mockito.verify(out, Mockito.times(1)).createEmptyBoard();
        // Now state must be equal to callInResetResult
        // We call getCurrentState. We want it to return a copy of the state, not thes
        // state array itself.
        // Run method under test
        final int[][] actualResult = out.getCurrentState();
        // Verify
        Mockito.verify(out, Mockito.times(2)).createEmptyBoard();
        // callInResetResult is the value of pisarenko.sudoku.InMemoryPersistence.state
        // We don't want getCurrentState to return it (so that other objects can't modify
        // it).
        Assertions.assertThat(actualResult).isNotSameAs(callInResetResult);
        // We want getCurrentState to return the copy of the state, which is
        // callIngetCurrentStateResult.
        Assertions.assertThat(actualResult).isSameAs(callIngetCurrentStateResult);
    }
    @Test
    public void putNumberDefaultScenario() {
        // Prepare
        final InMemoryPersistence out = new InMemoryPersistence();
        Assertions.assertThat(out.getCurrentState()).isEqualTo(InMemoryPersistence.INITIAL_STATE);
        putNumberTestLogic(out, 0, 0, 23);
        putNumberTestLogic(out, 4, 5, 15);
        putNumberTestLogic(out, 8, 8, 42);
    }

    protected void putNumberTestLogic(final InMemoryPersistence out,
                                      final int x,
                                      final int y,
                                      final int value) {
        // Run method under test
        out.putNumber(x, y, value);
        // Verify
        Assertions.assertThat(out.getCurrentState()[x][y]).isEqualTo(value);
    }
}
