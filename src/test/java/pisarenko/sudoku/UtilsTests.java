package pisarenko.sudoku;

import org.fest.assertions.api.Assertions;
import org.junit.Test;

/**
 * Created by pisarenko on 28.04.2016.
 */
public class UtilsTests {
    @Test
    public final void boardCompleteDetectsCompleteBoards() {
        Assertions.assertThat(Utils.boardComplete(DefaultSudokuMoveValidatorTests.COMPLETE_BOARD)).isTrue();
    }
    @Test
    public final void boardCompleteDetectsIncompleteBoards() {
        Assertions.assertThat(Utils.boardComplete(DefaultSudokuMoveValidatorTests.ALMOST_COMPLETE_BOARD)).isFalse();
        Assertions.assertThat(Utils.boardComplete(DefaultSudokuMoveValidatorTests.EVEN_LESS_COMPLETE_BOARD)).isFalse();
        Assertions.assertThat(Utils.boardComplete(InMemoryPersistence.INITIAL_STATE)).isFalse();
    }
    @Test
    public final void boardCompleteReturnsFalseOnZeroSizedBoard() {
        Assertions.assertThat(Utils.boardComplete(new int[0][0])).isFalse();
    }
}
