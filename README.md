Sudoku web service sample
=========================
Assumptions
-----------
* I assume that I don't have to implement
the support for several users and/or
several boards. That is, the usage
scenario is that there is always one
person playing on one board.

Services
--------
### Reset
* Sets the board into the initial state.
* Takes no parameters.
* Returns the new (resetted) board state.
### Move
* Makes the next move.
* Input: Coordinates of the cell and the number to put into that cell.
* Result: An enum indicating one of three possible outcomes:
  * The move is invalid. The board was not modified.
  * The move is valid, the board was modified, but it isn't complete yet.
  * The move is valid, the board was modified and now the Sudoku is solved.

Tests
-----
### Unit tests
The majority of the tests are unit tests and are located in the
directory `src/test/java`. There are 38 tests, which - according to
IntelliJ Idea - cover between 86 and 94 % of the code.

![Unit tests coverage](http://i.imgur.com/PZ5urRL.png)

### Integration test

There is also one integration test, which you can run by
doing the following steps.

#### Step 1: Unignore the test

Remove the `@Ignore` annotation of the test `pisarenko.sudoku.IntegrationTests.integrationTest`.

![Annotation in the integration test](http://i.imgur.com/oluaKrz.png)

#### Step 2: Launch the web service

Thereafter, launch the embedded Jetty web server by
entering the command `mvn jetty:run`. You can also
do it inside the IDE.

![Jetty launch configuration](http://i.imgur.com/QsOkSvw.png)

Wait, until you see the text `Started Jetty Server` in the console.

![Jetty server started](http://i.imgur.com/3ukHNvN.png)

#### Step 3: Run the integration test

Now you can run the integration test `pisarenko.sudoku.IntegrationTests.integrationTest`.
It will send one reset request to the running web service and several move requests.
It verifies that the web service returns HTTP status code 200 for all requests.
For all, but the last move request it validates that the result is *valid move,
board incomplete*. For the last move request, it checks that the web service
responds with *valid board, move complete*.
